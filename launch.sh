#!/bin/bash

#SBATCH --partition ict_cpu
#SBATCH --account brcluster
#SBATCH --reservation brcluster

IMAGE="runtime.sif"

srun --mpi=pmi2 -n $SLURM_JOB_NUM_NODES \
    singularity exec -B /petrobr $IMAGE $@
